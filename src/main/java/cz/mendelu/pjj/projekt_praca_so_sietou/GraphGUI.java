package cz.mendelu.pjj.projekt_praca_so_sietou;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Erik Prchlik
 * @version 0.1
 * Tento prvok prezentacnej vrstvy vytvara graficku reprezentaciu programu a urcuje co bude ake tlacidlo vykonavat.
 */
public class GraphGUI {
    /** @param  */
    private JTextField inputName;
    private JButton inputButton;
    private JTextArea inputNodes;
    private JButton proccessButton;
    private JButton editButton;
    private JTextArea resulte;
    private JTextField saveName;
    private JButton saveButton;
    private JTextArea inputRelationship;
    private JPanel input;
    private JPanel output;
    private JPanel edit;
    private JPanel rootPanel;
    private Data data=Main.getData();

    /** Konstruktor urcuje tlacitkam aku operaciu maju vykonavat */
    public GraphGUI() {
        saveButton.addActionListener(this::saveOutput);
        inputButton.addActionListener(this::openInput);
        editButton.addActionListener(this::editB);
        proccessButton.addActionListener(this::proccessB);
    }

    /** Metoda, ktora urcuje tlacidlu "proccessButton", ze ma do vysledneho textoveho pola ulozit vysledok spracovania dat z aplikacnej vrstvy */
    private void proccessB(ActionEvent e) {
        resulte.setText(Main.proccess());
    }

    /** Metoda, ktora po stlaceni "editButton" prepise data predtym urcene v datovej vrstve z ci uz upravenych alebo rovnakych dat. Jedna sa o data uzlov a vztahov */
    private void editB(ActionEvent e) {
        String[] stringsNodes = inputNodes.getText().split("\n");
        boolean nodes=true;
        data.edit(stringsNodes, nodes);
        String[] stringsRel = inputRelationship.getText().split("\n");
        nodes=false;
        data.edit(stringsRel, nodes);
        inputNodes.setText("");
        inputRelationship.setText("");
        this.setInputNodes();
        this.setInputRelationship();
    }

    /** Po stlaceni "inputButton" sa posle zisteny nazov suboru od uzivatela do datovej vrstvy a tam sa subor spracuje.
     * Nasledne, spracovane data sa vloza do textovych poly pre uzly a vztahy */
    private void openInput(ActionEvent e) {
        String fileName=inputName.getText();
        Pattern p = Pattern.compile("(.+).(txt)$");
        Matcher m = p.matcher(fileName);
        if (m.find()){
            fileName = m.group(1)+".txt";
            inputNodes.setText("");
            inputRelationship.setText("");
            data.readFile(fileName);
            this.setInputNodes();
            this.setInputRelationship();
        } else {
            Error.readFileE();
        }
    }

    /** Po stlaceni "saveButton" sa ulozi vysledok spracovaynch dat z datovej, aj aplikacnej vrstvy do suboru, ktoreho nazov je zisteny od uzivatela */
    private void saveOutput(ActionEvent e) {
        String name=saveName.getText();
        Pattern p = Pattern.compile("(.+).(txt)$");
        Matcher m = p.matcher(name);
        String output=resulte.getText();
        if (m.find()) {
            name = m.group(1) + ".txt";
            try (Writer w = new FileWriter(name)) {
                w.write(output);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    /** Tato metoda vytvori graficku reprezentaciu programu. */
    public void start(){
        JFrame jFrame =new JFrame("Graf");
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setContentPane(this.rootPanel);
        jFrame.setMinimumSize(new Dimension(800,600));
        jFrame.pack();
        jFrame.setLocation(100, 100); //levy horni roh bude na souradnici [100, 100]
        jFrame.setVisible(true);
    }

    /** Pomoocu tejto metodyy su ukladane vysledky spracovanych uzlov zo vstupnych dat */
    public void setInputNodes() {
        Object[] nodes = data.getNodes().toArray();
        for (int i=0 ;i<nodes.length;i++){
            this.inputNodes.append(nodes[i].toString());
            this.inputNodes.append("\n");
        }
    }

    /** Pomoocu tejto metodyy su ukladane vysledky spracovanych vztahov zo vstupnych dat */
    public void setInputRelationship() {
        Object[] relationships = data.getRelationship().toArray();
        for (int i=0 ;i<relationships.length;i++){
            this.inputRelationship.append(relationships[i].toString());
            this.inputRelationship.append("\n");
        }
    }
}
