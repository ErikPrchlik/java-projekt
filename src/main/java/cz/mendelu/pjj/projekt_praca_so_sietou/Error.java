package cz.mendelu.pjj.projekt_praca_so_sietou;


import javax.swing.*;

public class Error {

    /**
     * @author Erik Prchlik
     * @version 0.1
     * Definuje co sa ma stat, ak nastane chyba.
     */

    private static boolean err=false;
    private static final String NAME="Error";
    private static final String VYCET="Pri vycte uzlov neboli zadané: ";


    private Error() {
    }

    /** Staticka metoda, ktora bud vytvory modalne okno alebo vypise na err vystup urcitu chybu
     * @param s1 obsahuje nazov uzlu, ktory nebol najdeny v grafe
     * @param s2 obsahuje druhy nazov uzlu, ktory nebol najdeny v grafe
     */
    public static void relationshipE(String s1, String s2) {
        if (Main.getErr()){
            JFrame jFrame =new JFrame(NAME);
            JOptionPane.showMessageDialog(jFrame, VYCET + s1 + " a " + s2);
        } else {
            System.err.println(VYCET + s1 + " a " + s2);
            System.exit(0);
        }
        err=true;
    }

    /** Staticka metoda, ktora bud vytvory modalne okno alebo vypise na err vystup urcitu chybu
     * @param s obsahuje nazov uzlu, ktory nebol najdeny v grafe*/
    public static void relationshipE(String s) {
        if (Main.getErr()){
            JFrame jFrame =new JFrame(NAME);
            JOptionPane.showMessageDialog(jFrame, "Pri vycte uzlov nebol zadany: " + s);
        } else {
            System.err.println("Pri vycte uzlov nebol zadany: " + s);
            System.exit(0);
        }
        err=true;
    }

    /** Staticka metoda, ktora bud vytvory modalne okno alebo vypise na err vystup urcitu chybu
     * @param s obsahuje nazov pridavaneho uzlu*/
    public static void addNodeE(String s){
        if (Main.getErr()){
            JFrame jFrame =new JFrame(NAME);
            JOptionPane.showMessageDialog(jFrame, "Uzol: "+s+"\nNázov uzlu nesmie obsahovať \",\"");
        } else {
            System.err.println("Uzol: "+s);
            System.err.println("\nNázov uzlu nesmie obsahovať \",\"");
            System.exit(0);
        }
        err=true;
    }

    /** Staticka metoda, ktora bud vytvory modalne okno alebo vypise na err vystup urcitu chybu */
    public static void readFileE(){
        StringBuilder sb = new StringBuilder();
        sb.append("Nesprávne zadaný názov súboru!\n");
        sb.append("Návod:\n");
        sb.append("1. Názov súboru musí byť zadaný vo formáte \"nazov.txt\"\n");
        sb.append("2. Pri spustení programu ako parameter zadajte:\n");
        sb.append("       a) \"--gui\" - pre spustenie grafického rozhrania\n");
        sb.append("       b)  nič, pre zadávanie vstupov ručne vo formáte:\n");
        sb.append("                A\n");
        sb.append("                B\n\n");
        sb.append("                A,B\n");
        sb.append("           pre ukončenie zadávania vstupov požite Ctrl+d alebo Ctrl+z.");
        if (Main.getErr()){
            JFrame jFrame =new JFrame(NAME);
            JOptionPane.showMessageDialog(jFrame, sb.toString());
        } else {
            System.err.println(sb.toString());
            System.exit(0);
        }
        err=true;
    }

    /** @return Vracia, ci nastala chyba, alebo nie. */
    public static boolean getErr(){
        return err;
    }

    /** Nastavuje hodnotu, v prípade, ak nastala chyba a po jej spracovani nastavi false. */
    public static void setErr(){
        err=false;
    }
}
