package cz.mendelu.pjj.projekt_praca_so_sietou;


import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Erik Prchlik
 * @version 0.1
 * Zaistuje vytvaranie ostrovov grafu.
 */
public class Island extends Graph implements Comparable<Island>{
    private Set<String> nodesIs;

    /** Konstruktor definujuci typ mnoziny*/
    public Island() {
        this.nodesIs = new HashSet<>();
    }

    /** Konstruktor, ktory je vyuzivany pre vytvaranie ostrovu uz s urcenymi jeho uzlami
     * @param nodes je mnozina retazcov definujucich uzly ostrovu*/
    public Island(Set<String> nodes){
        this.nodesIs = new HashSet<>();
        this.nodesIs = nodes;
    }

    /** Tato metoda overuje, ci sa retazec (nazov uzlu) uz nachadza v ostrove, ktoreho je tato metoda volana. Ak ho neobsahuje, tak je zaroven pridana do mnozinu ozlov ostrovu.
     * @param s obsahuje nazov uzlu, ktora je nasledne overeny a popripade pridany k uzlom ostrova. */
    public void addNodeIs(String s){
        if (!nodesIs.contains(s)){
            nodesIs.add(s);
        }
    }

    /** Metoda vracia mnozinu uzlov patriaca ostorvu, ktora je urcena len na citanie.
     * @return Vracia nemodifikovatelnu mnozinu uzlov, ktoreobsahuje konkretny ostrov.*/
    public Collection<String> getNodesIs() {
        return Collections.unmodifiableSet(nodesIs);
    }

    @Override
    public int compareTo(Island o) {
        return 0;
    }
}
