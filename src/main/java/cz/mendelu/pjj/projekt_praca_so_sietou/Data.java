package cz.mendelu.pjj.projekt_praca_so_sietou;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Erik Prchlik
 * @version 0.1
 * Vstupy premiena na mnoziny uzlov a vztahov.
 */
public class Data {
    private Set<String> nodes;
    private Set<String> relationship;

    /**
     * Konstruktor iba vytvara mnoziny
     */
    public Data() {
        nodes = new HashSet<>();
        relationship = new HashSet<>();
    }

    /**
     * Metoda, ktora prijima vstupy z terminalu
     */
    public void readTerminal() {
        Scanner scanner = new Scanner(System.in);
        String line;
        boolean empty = false;
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            add(line, empty);
            if (line.trim().length() == 0) {
                empty = true;
            }
        }
    }

    /**
     * Metoda, ktora na zaklade vstupneho parametru cita vstupy zo suboru
     * @param fileName urcuje nazov suboru pre citanie
     */
    public void readFile(String fileName) {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            boolean empty = false;
            while ((line = br.readLine()) != null) {
                add(line, empty);
                if (line.trim().length() == 0) {
                    empty = true;
                }
            }
        } catch (FileNotFoundException e1) {
            Error.readFileE();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /** Metoda, ktora dostane prepise mnozinu nodes/relationship.
     *  Dochadza vlastne k znovu vytvoreniu mnoziny, pravdepodopbne kvoli uprave uzlov alebo ich vztahov.
     * @param nodes urcuje, ci je mnozina, ktora bude prepisana nodes/relationship
     * @param s je pole retazcov, ktore obsahuje prvky podla parametru nodes
     */
    public void edit(String[] s, boolean nodes) {
        Set<String> editSet;
        if (nodes) {
            editSet = this.nodes;
        } else {
            editSet = this.relationship;
        }
        editSet.clear();
        for (int i = 0; i < s.length; i++) {
            editSet.add(s[i]);
        }
    }

    /** Tato metoda sluzi k rozdeleniu retazca, kvoli urceniu nazvu dvoch uzlov vztahu
     *  @param s je retazec definujuci vztah, ktory musi byt rozdeleni pre jeho nasledne spracovanie
     *  @return vracia dvojprvkove pole obsahujuce nazvy uzlov definujucich vztah*/
    public String[] split(String s) {
        String[] strings = new String[2];
        Pattern p = Pattern.compile("^(.+),(.+)$");
        Matcher m = p.matcher(s);
        if (m.find()) {
            strings[0] = m.group(1);
            strings[1] = m.group(2);
        }
        return strings;
    }


    /** Metoda, ktora pridava retazce zo vstupov do mnoziny nodes/relationship na zaklade parametru empty
     * @param empty sluzi pre zistenie prazdneho radku
     *               prazdny riadok je dolezity pre rozdelenie uzlov a vztahov zo vstupu
     * @param line obsahuje retazec, ktory je pridany do mnoziny
     */
    private void add(String line, boolean empty) {
        if (!empty) {
            nodes.add(line);
        } else {
            relationship.add(line);
        }
    }

    /** Tato metoda vracia mnozinu vsetkych uzlov ziskanych zo vstupov, ktora je urcena len na citanie, inu operaciu nepovoli
     *  @return Vracia nemodifikovatelnu mnozinu vsetkych uzlov zo vstupu */
    public Collection<String> getNodes() {
        return Collections.unmodifiableSet(nodes);
    }

    /** Tato metoda vracia mnozinu vsetkych vztahov ziskanych zo vstupov, ktora je urcena len na citanie, inu operaciu nepovoli
     *  @return Vracia nemodifikovatelnu mnozinu vsetkych vztahov zo vstupu */
    public Collection<String> getRelationship() {
        return Collections.unmodifiableSet(relationship);
    }

}
