package cz.mendelu.pjj.projekt_praca_so_sietou;

/**
 * @author Erik Prchlik
 * @version 0.1
 * Na zaklade vstupov vyyhodnocuje pocet a stavbu ostrovov.
 */
public class Main {
    private static final Data DATA = getData();
    private static final GraphGUI GUI = getGUI();
    private static boolean err=false;

    /**
     * Metoda zabezpecuje spracovanie dat pripravenych v datovej vrstve.
     * V grafa sa vytvoria ostrovy definovane vztahmi
     *
     * @return Vracia retazec, ktory obsahuje informacie o pocte ostrovov a ich stavbe
     */
    public static String proccess() {
        GrafI i = new Graph();
        Object[] nodes = DATA.getNodes().toArray();
        for (int c = 0; c < nodes.length; c++) {
            i.addNode(nodes[c].toString());
        }
        String[] strings;
        Object[] relationships = DATA.getRelationship().toArray();
        for (int c = 0; c < relationships.length; c++) {
            strings = DATA.split(relationships[c].toString());
            i.relationship(strings[0], strings[1]);
        }
        i.createIslands();
        return i.print();
    }

    /**
     * vyhodnocovanie prezentacie programu
     *
     * @param args obsahuje hodnotu, podla ktorej sa zvoly nasledna podoba prezentacie programu
     *             ak neohsahuje nic, program bude citat vstupy z terminalu
     *             ak obsahuje retazec "--GUI" otvori sa graficka prezentacia programu
     *             ak obsahuje nieco ine, ocakava sa, že jeho obsah oznacuje nazov suboru, z ktoreho ma program citat
     */
    private static void execute(String[] args) {
        if (args.length == 0) {
            DATA.readTerminal();
            System.out.println(proccess());
        } else if (args[0].equalsIgnoreCase("--GUI")) {
            err=true;
            GUI.start();
        } else {
            DATA.readFile(args[0]);
            System.out.println(proccess());
        }
    }

    /**
     * premenna DATA je naplnena podla toho, ci uz obsahuje instanciu triedy Data alebo nie
     */
    public static Data getData() {
        return (DATA != null) ? DATA : new Data();
    }

    /**
     * premenna GUI je naplnena podla toho, ci uz obsahuje instanciu triedy GraphGUI alebo nie
     */
    private static GraphGUI getGUI() {
        return (GUI != null) ? GUI : new GraphGUI();
    }

    /** @return Vracia true, ak bol spustene graficke rozhranie.*/
    public static boolean getErr(){return err;}


    /**
     * metoda, ktora na zaklade premennej args spusti program
     */
    public static void main(String[] args) {
        execute(args);
    }
}
