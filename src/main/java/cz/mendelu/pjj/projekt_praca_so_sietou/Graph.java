package cz.mendelu.pjj.projekt_praca_so_sietou;

import java.util.*;

/**
 * @author Erik Prchlik
 * @version 0.1
 * Vytvara graf a urcuje podobu ostrovu.
 * Taktiez poskytuje konecny vysledok.
 */
public class Graph implements GrafI {
    protected Set<String> nodes;
    private List<Island> islands;
    private List<Set<String>> isNodes;

    /**
     * Konstruktor urcujuci typi mnoziny a zoznamov
     */
    public Graph() {
        nodes = new HashSet<>();
        islands = new ArrayList<>();
        isNodes = new ArrayList<>();
    }

    /**
     * Zaistuje pridavanie vsetkych uzlov do mnoziny ozlov. Kontroluje, ci uz uzlo obsahuje alebo, ci naov uzlu obsahuje ",".
     *
     * @param s obsahuje retazec, ktory reprezentuje nazov uzlu.
     */
    public void addNode(String s) {
        if (!s.contains(",")) {
            nodes.add(s);
        } else {
            Error.addNodeE(s);
        }
    }

    /**
     * Metoda urcena pre pre definovanie podoby ostorvu.
     *
     * @param s1 Je prvy nazov uzlu definujuci vztah
     * @param s2 Je druhy nazov uzlu definujuci vztah
     */
    public void relationship(String s1, String s2) {
        Set<String> set = null;
        if (this.error(s1, s2)) {
            if (!isNodes.isEmpty()) {
                for (int i = 0; i < isNodes.size(); i++) {
                    if ((isNodes.get(i).contains(s1)) || (isNodes.get(i).contains(s2))) {
                        set = isNodes.get(i);
                        break;
                    }
                }
            } else {
                Set<String> set1 = new HashSet<>();
                isNodes.add(set1);
                isNodes.get(isNodes.lastIndexOf(set1)).add(s1);
                isNodes.get(isNodes.lastIndexOf(set1)).add(s2);
                return;
            }
            if (set != null) {
                set.add(s1);
                set.add(s2);
            } else {
                set = new HashSet<>();
                isNodes.add(set);
                isNodes.get(isNodes.lastIndexOf(set)).add(s1);
                isNodes.get(isNodes.lastIndexOf(set)).add(s2);
            }
        }
    }

    /** Zistuje ci su spravne zadane vstupy.
     * @param s1 1. vstup vztahu
     * @param s2 2. vstup vztahu
     * @return Vracia true ak nastala chyba pri vstupoch, alebo false, ak sa moze vztah spracovat.*/
    public boolean error(String s1, String s2) {
        if (!nodes.contains(s1)) {
            if (!nodes.contains(s2)) {
                Error.relationshipE(s1, s2);
            } else {
                Error.relationshipE(s1);
            }
        } else if (!nodes.contains(s2)) {
            Error.relationshipE(s2);
        } else {
            return true;
        }
        return false;
    }

    /**
     * Metoda, ktora vytvara ostrovy uz z voprad pripraveneho zoznamu mnozin uzlov urcitych ostrovov.
     */
    public void createIslands() {
        for (int i = 0; i < isNodes.size(); i++) {
            islands.add(new Island(isNodes.get(i)));
        }
    }

    /**
     * Tato metoda hlada uzly, ktore neobsahuje zaden ostrov a vytvori z nich samostatne ostrovi.
     */
    public void findSingleNodes() {
        boolean contain = true;
        Object[] nodesO = this.nodes.toArray();
        for (int c = 1; c < nodesO.length; c++) {
            String s = nodesO[c].toString();
            for (Island is : islands) {
                if (!is.getNodesIs().contains(s)) {
                    contain = false;
                } else {
                    contain = true;
                    break;
                }
            }
            if (!contain) {
                Island is1 = new Island();
                is1.addNodeIs(s);
                islands.add(is1);
            }
        }
    }

    /**
     * Metoda spracuva vsetky vysledky a jednoti ich do jedneho retazca.
     *
     * @return Vracia retazec obsahujuci celkovi pocet ostrovov a ich stavbu.
     */
    public String print() {
        if (!Error.getErr()) {
            StringBuilder sb = new StringBuilder();
            findSingleNodes();
            Collections.sort(islands);
            Collections.reverse(islands);
            sb.append(islands.size());
            sb.append("\n\n");
            for (int i = 0; i < islands.size(); i++) {
                sb.append(islands.get(i).getNodesIs());
                sb.append("\n");
            }
            return sb.toString();
        }
        Error.setErr();
        return "";
    }
}
