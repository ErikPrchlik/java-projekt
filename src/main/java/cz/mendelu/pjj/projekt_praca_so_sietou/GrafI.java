package cz.mendelu.pjj.projekt_praca_so_sietou;

/**
 * @author Erik Prchlik
 * @version 0.1
 * Vytvara viditelne rozhranie pre triedy Island a Graph.
 */
public interface GrafI {
    /** Zaistuje pridavanie vsetkych uzlov do mnoziny ozlov. Kontroluje, ci uz uzlo obsahuje alebo, ci naov uzlu obsahuje ",". */
    void addNode(String s);
/** Metoda urcena pre pre definovanie podoby ostorvu.*/
    void relationship(String s1, String s2);
    /** Metoda, ktora vytvara ostrovy uz z voprad pripraveneho zoznamu mnozin uzlov urcitych ostrovov.*/
    void createIslands();
/** Metoda spracuva vsetky vysledky a jednoti ich do jedneho retazca.*/
    String print();
}
